/*
 * home_security.h
 *
 *  Created on: Dec 14, 2020
 *      Author: harrisonthorne
 */

#ifndef HOME_SECURITY_H_
#define HOME_SECURITY_H_

#include "aliases.h"
#include "display.h"
#include "msp.h"

enum State {
    Idle,
    Alarm,
    GuessPass,
    NewPass,
} state = Idle;

enum State get_state(void);
void attempt_unlock(void);
void disable_alarm(void);
void input_number(uint8_t key);
void say_prompt_guess_password(void);
void say_prompt_new_password(void);
void save_new_password(void);
void say_disabled(void);
void say_incorrect(void);
void say_ready(void);
void say_unlocked(void);
void signal_alarm(void);
void start_guess_password(void);
void start_new_password(void);
void turn_off_LED2(void);

int i = 0, i_pass = 0;
int locked = 0;
int current_pass[10], temp_pass[10];
int incorrect_attempts = 0;
int attempts_allowed = 3;

void turn_off_LED2(void) {
    P2->OUT &= ~(LED2RED|LED2GREEN|LED2BLUE); // turn off the RGB
}

void signal_alarm(void) {
    state = Alarm;
    P1->OUT |= LED1; // turn on the red LED1
    say_disabled();
}

void disable_alarm(void) {
    // reset state and turn off LEDs
    state = Idle;
    P1->OUT &= ~LED1;
    turn_off_LED2();
    say_ready();
}

void attempt_unlock(void) {
    if (state == GuessPass){
        for (i = 0; i < 10; i++) {
            if (current_pass[i] != temp_pass[i]) {
                // if any part of the password doesn't match, the password isn't
                // wrong. we'll signal this with a red LED2. if we've exceeded
                // the incorrect attempt count, the alarm will be signalled and
                // LED1 will also glow red.

                state = Idle;
                locked = 1;
                turn_off_LED2();
                P2->OUT |= LED2RED; // turn on the red LED (P2.0)
                incorrect_attempts++;

                if (incorrect_attempts == attempts_allowed) {
                    // too many wrong passwords! sound the alarm!!!!!!!!!!!!
                    signal_alarm();
                } else {
                    // wrong password, tell the user
                    say_incorrect();
                }
                return;
            }
        }

        // if we made it through the loop without a return, the unlock was
        // successful :)
        state = Idle;
        locked = 0;

        // turn on the green LED
        turn_off_LED2();
        P2->OUT |= LED2GREEN; 

        // reset incorrect attempt count
        incorrect_attempts = 0;

        // tell the user
        say_unlocked();
    }
}

void save_new_password(void) {
    if(state == NewPass) {
        for(i = 0; i < 10; i++) {
            current_pass[i] = temp_pass[i];
        }

        turn_off_LED2();
        P2->OUT |= LED2BLUE; // turn on the blue LED (P2.2)
        state = Idle;
        locked = 1;

        say_ready();
    }
}

void input_number(uint8_t key) {
    // handle the (number) key passed in. if we're entering a new password,
    // append the key to the end of the password. if guessing the password,
    // append the key to the end of the guessed password.
    if (state == NewPass || state == GuessPass){
        // make sure we're not out of bounds
        if (i_pass < 10) {
            temp_pass[i_pass++] = key;

            if (i_pass == 10 && state == NewPass) {
                save_new_password();
            }
        }

        // write a lil asterisk to the display
        display_write(CHAR_ASTERISK);
    }
}

void start_new_password(void) {
    // only allow new password if system is idle and unlocked
    if (state == Idle && locked == 0) {
        // reset password
        for (i = 0; i < 10; i++) {
            temp_pass[i] = -1;
        }

        // change state, reset password index, turn off LED2
        state = NewPass;
        i_pass = 0;
        turn_off_LED2();

        say_prompt_new_password();
    }
}

void start_guess_password(void) {
    // system must be idle
    if (state == Idle) {
        // if locked, user must guess the password. otherwise, the system is
        // locked by the user
        if (locked == 1) {
            // tell the user to enter a new password
            say_prompt_guess_password();

            // reset temp password
            for (i = 0; i < 10; i++) {
                temp_pass[i] = -1;
            }

            // change state, reset password index, turn off LED2
            state = GuessPass;
            i_pass = 0;
            turn_off_LED2();
        } else {
            locked = 1;
            turn_off_LED2();
            P2->OUT |= LED2BLUE;
            say_ready();

            // the state will not change in this case. the system was idle and
            // will remain idle
        }
    }
}

// hey,
// let's be honest here.
// there are WAY better ways to display characters on the display. this is
// stupid. but it works for now. if i have time, i will implement a
// string-to-display function.

// tells the user to enter a new password
void say_prompt_new_password(void) {
    // start with a fresh display
    clear_display();

    // Enter new pass:
    display_write(CHAR_E);
    display_write(CHAR_N);
    display_write(CHAR_T);
    display_write(CHAR_E);
    display_write(CHAR_R);
    display_write(CHAR_SPACE);
    display_write(CHAR_N);
    display_write(CHAR_E);
    display_write(CHAR_W);
    display_write(CHAR_SPACE);
    next_line(); // "pass:" won't fit on same line (but pass without the colon would >:(
    display_write(CHAR_P);
    display_write(CHAR_A);
    display_write(CHAR_S);
    display_write(CHAR_S);
    display_write(CHAR_COLON);

    // blank line, password line, blank line, then write on next line
    advance_lines(4);

    // B: Submit
    display_write(CHAR_B);
    display_write(CHAR_COLON);
    display_write(CHAR_S);
    display_write(CHAR_U);
    display_write(CHAR_B);
    display_write(CHAR_M);
    display_write(CHAR_I);
    display_write(CHAR_T);

    // retreats back to the password row
    retreat_lines(2);
}

// tells the user to enter the password
void say_prompt_guess_password(void) {
    // start with a fresh display
    clear_display();

    // Enter pass
    display_write(CHAR_E);
    display_write(CHAR_N);
    display_write(CHAR_T);
    display_write(CHAR_E);
    display_write(CHAR_R);
    display_write(CHAR_SPACE);
    display_write(CHAR_P);
    display_write(CHAR_A);
    display_write(CHAR_S);
    display_write(CHAR_S);
    display_write(CHAR_COLON);

    // blank line, password line, blank line, then write on next line
    advance_lines(4);

    // D: Submit
    display_write(CHAR_D);
    display_write(CHAR_COLON);
    display_write(CHAR_S);
    display_write(CHAR_U);
    display_write(CHAR_B);
    display_write(CHAR_M);
    display_write(CHAR_I);
    display_write(CHAR_T);
    
    // retreats back to the password row
    retreat_lines(2);
}

// tells the user the system is ready (locked)
void say_ready(void) {
    // start with a fresh display
    clear_display();

    // Ready!
    display_write(CHAR_R);
    display_write(CHAR_E);
    display_write(CHAR_A);
    display_write(CHAR_D);
    display_write(CHAR_Y);
    display_write(CHAR_EXCLAMATION);

    // add blank line
    next_paragraph();

    // add a lil smiley face, ya know, to gain the trust of the humans
    display_write(CHAR_SMILEY_LEFT);
    display_write(CHAR_SMILEY_RIGHT);

    // another blank line
    next_paragraph();

    // C: Unlock
    display_write(CHAR_C);
    display_write(CHAR_COLON);
    display_write(CHAR_U);
    display_write(CHAR_N);
    display_write(CHAR_L);
    display_write(CHAR_O);
    display_write(CHAR_C);
    display_write(CHAR_K);
}

// tells the user the password is wrong
void say_incorrect(void) {
    // start with a fresh display
    clear_display();

    // Wrong pass!
    display_write(CHAR_W);
    display_write(CHAR_R);
    display_write(CHAR_O);
    display_write(CHAR_N);
    display_write(CHAR_G);
    display_write(CHAR_SPACE);
    display_write(CHAR_P);
    display_write(CHAR_A);
    display_write(CHAR_S);
    display_write(CHAR_S);
    display_write(CHAR_EXCLAMATION);

    // add blank line
    next_paragraph();

    // X tries left
    display_write(num_to_display(attempts_allowed - incorrect_attempts));
    display_write(CHAR_SPACE);
    display_write(CHAR_T);
    display_write(CHAR_R);
    if (attempts_allowed - incorrect_attempts == 1) {
        display_write(CHAR_Y);
    } else {
        display_write(CHAR_I);
        display_write(CHAR_E);
        display_write(CHAR_S);
    }
    display_write(CHAR_SPACE);
    display_write(CHAR_L);
    display_write(CHAR_E);
    display_write(CHAR_F);
    display_write(CHAR_T);

    // blank line
    next_paragraph();

    // C: Try again
    display_write(CHAR_C);
    display_write(CHAR_COLON);
    display_write(CHAR_T);
    display_write(CHAR_R);
    display_write(CHAR_Y);
    display_write(CHAR_SPACE);
    display_write(CHAR_A);
    display_write(CHAR_G);
    display_write(CHAR_A);
    display_write(CHAR_I);
    display_write(CHAR_N);
}

// tells the user the system is unlocked. they can enter a new password if they
// want
void say_unlocked(void) {
    // start with a fresh display
    clear_display();

    // Hello! :) System is unlocked
    display_write(CHAR_H);
    display_write(CHAR_E);
    display_write(CHAR_L);
    display_write(CHAR_L);
    display_write(CHAR_O);
    display_write(CHAR_EXCLAMATION);
    display_write(CHAR_SMILEY_LEFT);
    display_write(CHAR_SMILEY_RIGHT);
    next_line(); // "system" won't fit
    display_write(CHAR_S);
    display_write(CHAR_Y);
    display_write(CHAR_S);
    display_write(CHAR_T);
    display_write(CHAR_E);
    display_write(CHAR_M);
    display_write(CHAR_SPACE);
    display_write(CHAR_I);
    display_write(CHAR_S);
    next_line(); // "unlocked" won't fit
    display_write(CHAR_U);
    display_write(CHAR_N);
    display_write(CHAR_L);
    display_write(CHAR_O);
    display_write(CHAR_C);
    display_write(CHAR_K);
    display_write(CHAR_E);
    display_write(CHAR_D);

    // add blank line
    next_paragraph();

    // A: New pass
    display_write(CHAR_A);
    display_write(CHAR_COLON);
    display_write(CHAR_N);
    display_write(CHAR_E);
    display_write(CHAR_W);
    display_write(CHAR_SPACE);
    display_write(CHAR_P);
    display_write(CHAR_A);
    display_write(CHAR_S);
    display_write(CHAR_S);

    // NO blank line
    next_line();

    // C: lock
    display_write(CHAR_C);
    display_write(CHAR_COLON);
    display_write(CHAR_L);
    display_write(CHAR_O);
    display_write(CHAR_C);
    display_write(CHAR_K);
}

// tells the user the system is disabled (alarm state). we won't give
// instructions on how to disable the alarm
void say_disabled(void) {
    // start with a fresh display
    clear_display();

    // Too many bad attempts
    display_write(CHAR_T);
    display_write(CHAR_O);
    display_write(CHAR_O);
    display_write(CHAR_SPACE);
    display_write(CHAR_M);
    display_write(CHAR_A);
    display_write(CHAR_N);
    display_write(CHAR_Y);
    display_write(CHAR_SPACE);
    display_write(CHAR_B);
    display_write(CHAR_A);
    display_write(CHAR_D);
    next_line(); // "attempts" won't fit
    display_write(CHAR_A);
    display_write(CHAR_T);
    display_write(CHAR_T);
    display_write(CHAR_E);
    display_write(CHAR_M);
    display_write(CHAR_P);
    display_write(CHAR_T);
    display_write(CHAR_S);

    next_paragraph();

    // Alarm is on
    display_write(CHAR_A);
    display_write(CHAR_L);
    display_write(CHAR_A);
    display_write(CHAR_R);
    display_write(CHAR_M);
    display_write(CHAR_SPACE);
    display_write(CHAR_I);
    display_write(CHAR_S);
    display_write(CHAR_SPACE);
    display_write(CHAR_O);
    display_write(CHAR_N);
    
    next_paragraph();

    // Unlocking is disabled
    display_write(CHAR_U);
    display_write(CHAR_N);
    display_write(CHAR_L);
    display_write(CHAR_O);
    display_write(CHAR_C);
    display_write(CHAR_K);
    display_write(CHAR_I);
    display_write(CHAR_N);
    display_write(CHAR_G);
    display_write(CHAR_SPACE);
    display_write(CHAR_O);
    display_write(CHAR_F);
    display_write(CHAR_F);
}

// only works for numbers up to 9. converts a primitive int to a display font
// code
unsigned int num_to_display(unsigned int i) {
    // 0 => 32 ... 9 => 41
    return (i % 10) + 32;
}

enum State get_state(void) {
    return state;
}

#endif /* HOME_SECURITY_H_ */
