#include "msp.h"
#include "display.h"
#include "io.h"
#include "interrupts.h"

void init_io(void);
void init_interrupts(void);
void init_display(void);
void say_setup(void);

int main (void) {

    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD; // hold the watchdog timer

    init_io();
    init_interrupts();
    init_display();

    // initially, we'll tell the user to press A to set up
    say_setup();

    // hold the program to let it run. if main returns, the program ends.
    // interrupts should handle the logic of the program. see 'interrupts.h'
    // for (some of) the magic
    while (1);
}

void init_io(void) {
    P1->DIR |= BIT0;                          // set up pin P1.0 (red LED) as output
    P2->DIR |= (BIT0|BIT1|BIT2);              // set up pins P2.0, P2.1, P2.2 (R, G, and B LEDs) as output
    P1->OUT &= ~LED1;                         // turn off red LED
    P2->OUT &= ~(LED2RED|LED2GREEN|LED2BLUE); // turn off RGB LED

    P4->DIR &= ~(BIT0 | BIT1 | BIT2 | BIT3); // set up pins P4.0, P4.1, P4.2, P4.3 (pins for the input from the keypad controller) as input

    P1->DIR &= ~S1; // set up pin P1.1 (S1) as input
    P1->REN |= S1;  // connect pull resistor to pin P1.1 (S1)
    P1->OUT |= S1;  // configure pull resistor as pull up
}

void init_interrupts(void) {
    P1->IFG &= ~S1; // clear the interrupt flag for pin P1.1 (S1)
    P1->IE |= S1;   // enable the interrupt for pin P1.1 (S1)

    P3->DIR &= ~BIT0; // set up pin P3.0 as input
    P3->REN |= BIT0;  // connect pull resistor to pin P3.0
    P3->OUT |= BIT0;  // configure pull resistor as pull up
    P3->IFG &= ~BIT0; // clear interrupt flag for pin P3.0
    P3->IE |= BIT0;   // enable the interrupt for pin P3.0

    NVIC->ISER[1] |= 0x20; // enable port 3 interrupts
    NVIC->ISER[1] |= 0x08; // enable port 1 interrupts
    _enable_interrupts();  // does what it says lol
}

void init_display(void) {
    GLCD_init(); // initialize the GLCD controller
    GLCD_clear(); // clear display and  home the cursor
}

void say_setup(void) {
    // Hello :)
    display_write(CHAR_H);
    display_write(CHAR_E);
    display_write(CHAR_L);
    display_write(CHAR_L);
    display_write(CHAR_O);
    display_write(CHAR_SMILEY_LEFT);
    display_write(CHAR_SMILEY_RIGHT);

    next_paragraph();

    // Press A to setup
    display_write(CHAR_P);
    display_write(CHAR_R);
    display_write(CHAR_E);
    display_write(CHAR_S);
    display_write(CHAR_S);
    display_write(CHAR_SPACE);
    display_write(CHAR_A);
    display_write(CHAR_SPACE);
    display_write(CHAR_T);
    display_write(CHAR_O);
    display_write(CHAR_SPACE);
    display_write(CHAR_S);
    display_write(CHAR_E);
    display_write(CHAR_T);
    display_write(CHAR_U);
    display_write(CHAR_P);

    next_paragraph();

    // A: New pass
    display_write(CHAR_A);
    display_write(CHAR_COLON);
    display_write(CHAR_N);
    display_write(CHAR_E);
    display_write(CHAR_W);
    display_write(CHAR_SPACE);
    display_write(CHAR_P);
    display_write(CHAR_A);
    display_write(CHAR_S);
    display_write(CHAR_S);
}
