/*
 * io.h
 *
 *  Created on: Dec 14, 2020
 *      Author: harrisonthorne
 */

#ifndef IO_H_
#define IO_H_

#include "msp.h"

/***
 * keypad decoder function
 ***/
uint8_t keypad_decode(){
    uint8_t key = 0;
    uint8_t port = 0;

    port += P4->IN & BIT0; /* input value from pin P4.0 */
    port += P4->IN & BIT1; /* input value from pin P4.1 */
    port += P4->IN & BIT2; /* input value from pin P4.2 */
    port += P4->IN & BIT3; /* input value from pin P4.3 */

    switch (port) {
        case 0x0D: key = 0x0; break; /* 0 */
        case 0x00: key = 0x1; break; /* 1 */
        case 0x01: key = 0x2; break; /* 2 */
        case 0x02: key = 0x3; break; /* 3 */
        case 0x04: key = 0x4; break; /* 4 */
        case 0x05: key = 0x5; break; /* 5 */
        case 0x06: key = 0x6; break; /* 6 */
        case 0x08: key = 0x7; break; /* 7 */
        case 0x09: key = 0x8; break; /* 8 */
        case 0x0A: key = 0x9; break; /* 9 */
        case 0x03: key = 0xA; break; /* A */
        case 0x07: key = 0xB; break; /* B */
        case 0x0B: key = 0xC; break; /* C */
        case 0x0F: key = 0xD; break; /* D */
        case 0x0C: key = 0xE; break; /* * */
        case 0x0E: key = 0xF; break; /* # */
    }

    return key;
}

#endif /* IO_H_ */
