/*
 * display.h
 *
 *  Created on: Dec 14, 2020
 *      Author: harrisonthorne
 */

#ifndef DISPLAY_H_
#define DISPLAY_H_

#define CE  0x01    // P6.0 chip select
#define RESET 0x40  // P6.6 reset
#define DC 0x80     // P6.7 register select

// define the pixel size of display
#define GLCD_WIDTH  84
#define GLCD_HEIGHT 48

#define CHAR_WIDTH 6
#define CHAR_HEIGHT 8

void GLCD_setCursor(unsigned char x, unsigned char y);
void GLCD_clear(void);
void GLCD_init(void);
void GLCD_data_write(unsigned char data);
void GLCD_command_write(unsigned char data);
void GLCD_putchar(int c);
void SPI_init(void);
void SPI_write(unsigned char data);
void puthelloworld(void);

#define CHAR_SPACE 0
#define CHAR_A 1
#define CHAR_B 2
#define CHAR_C 3
#define CHAR_D 4
#define CHAR_E 5
#define CHAR_F 6
#define CHAR_G 7
#define CHAR_H 8
#define CHAR_I 9
#define CHAR_J 10
#define CHAR_K 11
#define CHAR_L 12
#define CHAR_M 13
#define CHAR_N 14
#define CHAR_O 15
#define CHAR_P 16
#define CHAR_Q 17
#define CHAR_R 18
#define CHAR_S 19
#define CHAR_T 20
#define CHAR_U 21
#define CHAR_V 22
#define CHAR_W 23
#define CHAR_X 24
#define CHAR_Y 25
#define CHAR_Z 26
#define CHAR_EXCLAMATION 27
#define CHAR_SMILEY_LEFT 28
#define CHAR_SMILEY_RIGHT 29
#define CHAR_ASTERISK 30
#define CHAR_COLON 31
#define CHAR_0 32
#define CHAR_1 33
#define CHAR_2 34
#define CHAR_3 35
#define CHAR_4 36
#define CHAR_5 37
#define CHAR_6 38
#define CHAR_7 39
#define CHAR_8 40
#define CHAR_9 41

// the row/line to write on
int line;

// keeps track of how many chars have been written, in case text overflows into
// next line
int numChars;

// sample font table
const char font_table[][6] = {
    {0x00, 0x00, 0x00, 0x00, 0x00, 0x00},  // 0: blank
    {0x7e, 0x11, 0x11, 0x11, 0x7e, 0x00},  // 1: A
    {0x7f, 0x49, 0x49, 0x49, 0x36, 0x00},  // 2: B
    {0x3e, 0x41, 0x41, 0x41, 0x22, 0x00},  // 3: C
    {0x7f, 0x41, 0x41, 0x41, 0x3E, 0x00},  // 4: D
    {0x7f, 0x49, 0x49, 0x49, 0x41, 0x00},  // 5: E
    {0x7f, 0x09, 0x09, 0x09, 0x01, 0x00},  // 6: F
    {0x3e, 0x41, 0x49, 0x49, 0x7a, 0x00},  // 7: G
    {0x7f, 0x08, 0x08, 0x08, 0x7f, 0x00},  // 8: H
    {0x41, 0x41, 0x7f, 0x41, 0x41, 0x00},  // 9: I
    {0x20, 0x40, 0x40, 0x40, 0x3f, 0x00},  // 10: J
    {0x7f, 0x08, 0x14, 0x22, 0x41, 0x00},  // 11: K
    {0x7f, 0x40, 0x40, 0x40, 0x40, 0x00},  // 12: L
    {0x7f, 0x02, 0x0c, 0x02, 0x7f, 0x00},  // 13: M
    {0x7f, 0x04, 0x08, 0x10, 0x7f, 0x00},  // 14: N
    {0x3e, 0x41, 0x41, 0x41, 0x3e, 0x00},  // 15: O
    {0x7f, 0x09, 0x09, 0x09, 0x06, 0x00},  // 16: P
    {0x3e, 0x41, 0x51, 0x61, 0x7e, 0x00},  // 17: Q
    {0x7f, 0x09, 0x19, 0x29, 0x46, 0x00},  // 18: R
    {0x26, 0x49, 0x49, 0x49, 0x32, 0x00},  // 19: S
    {0x01, 0x01, 0x7f, 0x01, 0x01, 0x00},  // 20: T
    {0x3f, 0x40, 0x40, 0x40, 0x3f, 0x00},  // 21: U
    {0x1f, 0x20, 0x40, 0x20, 0x1f, 0x00},  // 22: V
    {0x3f, 0x40, 0x38, 0x40, 0x3f, 0x00},  // 23: W
    {0x63, 0x14, 0x08, 0x14, 0x63, 0x00},  // 24: X
    {0x03, 0x04, 0x78, 0x04, 0x03, 0x00},  // 25: Y
    {0x61, 0x51, 0x49, 0x45, 0x43, 0x00},  // 26: Z
    {0x00, 0x00, 0x5f, 0x00, 0x00, 0x00},  // 27: !
    {0x7e, 0x81, 0xb5, 0xa1, 0xa1, 0xb5},  // 28: smiley left
    {0x81, 0x7e, 0x00, 0x00, 0x00, 0x00},  // 29: smiley right
    {0x22, 0x14, 0x7f, 0x14, 0x22, 0x00},  // 30: asterisk
    {0x44, 0x00, 0x00, 0x00, 0x00, 0x00},  // 31: colon
    {0x3e, 0x51, 0x49, 0x45, 0x3e, 0x00}, // 32: 0
    {0x00, 0x42, 0x7f, 0x40, 0x00, 0x00}, // 33: 1
    {0x42, 0x61, 0x51, 0x49, 0x46, 0x00}, // 34: 2
    {0x22, 0x41, 0x49, 0x49, 0x36, 0x00}, // 35: 3
    {0x18, 0x14, 0x12, 0x7f, 0x10, 0x00}, // 36: 4
    {0x4f, 0x49, 0x49, 0x49, 0x31, 0x00}, // 37: 5
    {0x3e, 0x49, 0x49, 0x49, 0x32, 0x00}, // 38: 6
    {0x01, 0x01, 0x71, 0x09, 0x07, 0x00}, // 39: 7
    {0x36, 0x49, 0x49, 0x49, 0x36, 0x00}, // 40: 8
    {0x26, 0x49, 0x49, 0x49, 0x3e, 0x00}  // 41: 9
};

void GLCD_putchar(int c) {
    int i;
    for(i = 0; i < 6; i++) {
        GLCD_data_write(font_table[c][i]);
    }
    numChars++;

    // line may need to be advanced automatically if the number of chars written
    // to one line exceeds the line width
    line += numChars / (GLCD_WIDTH / CHAR_WIDTH);
    numChars %= GLCD_WIDTH / CHAR_WIDTH;
}

void GLCD_setx(unsigned char x) {
    GLCD_command_write(0x80 | x); // column
}

void GLCD_sety(unsigned char y) {
    line = y;
    numChars = 0;
    GLCD_command_write(0x40 | y); // bank (6 rows per bank)
}

void GLCD_setCursor(unsigned char x, unsigned char y) {
    GLCD_setx(x);
    GLCD_sety(y);
}

// clears the GLCD by writing zeros to the entire screen
void GLCD_clear(void) {
    int32_t i;
    for (i = 0; i < (GLCD_WIDTH * GLCD_HEIGHT / 6); i++) {
        GLCD_data_write(0x00);
    }
    GLCD_setCursor(0, 0); // return cursor to top left
}

// send the initialization commands to PCD8544 GLCD controller
void GLCD_init(void) {
    SPI_init();

    // hardware reset of GLCD controller
    P6->OUT |= RESET; // deasssert reset

    GLCD_command_write(0x21); // set extended command mode
    GLCD_command_write(0xB8); // set LCD Vop for contrast
    GLCD_command_write(0x04); // set temp coefficient
    GLCD_command_write(0x14); // set LCD bias mode 1:48
    GLCD_command_write(0x20); // set normal command mode
    GLCD_command_write(0x0C); // set display normal mode
}

// write to GLCD controller data register
void GLCD_data_write(unsigned char data) {
    P6->OUT |= DC;              // select data register
    SPI_write(data);            // send data via SPI
}

// write to GLCD controller command register
void GLCD_command_write(unsigned char data) {
    P6->OUT &= ~DC; // select command register
    SPI_write(data); // send data via SPI
}

void SPI_init(void) {
    EUSCI_B0->CTLW0 = 0x0001; // put UCB0 in reset mode
    EUSCI_B0->CTLW0 = 0x69C1; // PH=0, PL=1, MSB first, Master, SPI, SMCLK
    EUSCI_B0->BRW = 3; // 3 MHz / 3 = 1MHz
    EUSCI_B0->CTLW0 &= ~0x001; // enable UCB0 after config

    P1->SEL0 |= 0x60; // P1.5, P1.6 for UCB0
    P1->SEL1 &= ~0x60;

    P6->DIR |= (CE | RESET | DC); // P6.7, P6.6, P6.0 set as output
    P6->OUT |= CE; // CE idle high
    P6->OUT &= ~RESET; // assert reset
}

void SPI_write(unsigned char data) {
    P6->OUT &= ~CE; // assert /CE
    EUSCI_B0->TXBUF = data; // write data
    while (EUSCI_B0->STATW & 0x01); // wait for transmission to complete
    P6->OUT |= CE; // deassert /CE
}

// just a little alias to make the source code look a little better. (this is
// more self-documenting)
void display_write(int c) {
    GLCD_putchar(c);
}

// specifies which row to set the cursor at. sugar for "GLCD_sety"
void set_line(unsigned char i) {
    GLCD_sety(i);
}

// advances i rows down and moves the cursor to the beginning of the line
void advance_lines(unsigned char i) {
    GLCD_setCursor(0, line + i);
}

// retreats i rows up and moves the cursor to the beginning of the line
void retreat_lines(unsigned char i) {
    // this ensures we don't go below zero, because that would be bad
    if (i > line) {
        i = line;
    }

    GLCD_setCursor(0, line - i);
}

// moves the cursor to the beginning of the next line
void next_line(void) {
    advance_lines(1);
}

// advances two lines to add a blank line between messages
void next_paragraph(void) {
    advance_lines(2);
}

// alias/sugar for GLCD_clear
void clear_display(void) {
    GLCD_clear();
}

#endif // DISPLAY_H_
