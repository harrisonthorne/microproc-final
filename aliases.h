/*
 * aliases.h
 *
 *  Created on: Dec 14, 2020
 *      Author: harrisonthorne
 */

#ifndef ALIASES_H_
#define ALIASES_H_

#include "msp.h"

#define LED1 BIT0
#define LED2RED BIT0
#define LED2GREEN BIT1
#define LED2BLUE BIT2
#define S1 BIT1

#endif /* ALIASES_H_ */
