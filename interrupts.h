/*
 * interrupts.h
 *
 *  Created on: Dec 14, 2020
 *      Author: harrisonthorne
 */

#ifndef INTERRUPTS_H_
#define INTERRUPTS_H_

#include "aliases.h"
#include "home_security.h"

// interrupt handling for port 1. disables the alarm state, if present.
// otherwise, the button just turns off LED2, lol
void PORT1_IRQHandler(void){

    uint32_t status;

    status = P1->IFG;    /* get the interrupt status for port 1 */
    P1->IFG &= ~BIT1;    /* clear the interrupt for port 1, pin 1 */

    if(status & BIT1){   /* constant for the pin 1 mask */
        disable_alarm();
    }
}

// interrupt handling for port 3
void PORT3_IRQHandler(void){

    uint32_t status;

    uint8_t key = 0;

    status = P3->IFG; // get the interrupt status for port 3
    P3->IFG &= ~BIT0; // clear the interrupt for port 3, pin 0

    if(status & BIT0){  // constant for the pin 0 mask
        key = keypad_decode();
        if (get_state() != Alarm) {
            if(key == 0xA){
                start_new_password();
            } else if (key == 0xB) {
                save_new_password();
            } else if (key == 0xC) {
                start_guess_password();
            } else if (key == 0xD) {
                attempt_unlock();
            } else if (key == 0xE) {
            } else if (key == 0xF) {
            } else {
                input_number(key); 
            }
        }
    }
}

#endif /* INTERRUPTS_H_ */
