---
title: "ECEN 260: Final Project"
subtitle: "Enhanced Home Security"
author: Harrison Thorne
date: \today
urlcolor: cyan
...

What is this?
============================================================

This Enhanced Home Security project more or less simulates
the terminal of a home security system. The user can set a
password, lock the system at will, and unlock the system to
change settings.

The project builds off of the concepts we've learned in
class by combining digital I/O, interrupts, and a display.

How is this different from Week 7's lab?
------------------------------------------------------------

The main goal of this project was to vastly improve the
functionality, maintenance, and user experience of the home
security system from week 7. 

The main problem of having a home security system without a
user interface is that the user has *no idea* of what state
the system is in, how many numbers have been entered for a
password, and ultimately, if they've even pressed a button
(because sometimes key presses on the keypad don't register
so well).

Here's a brief outline of everything the project aims to
improve from week 7:

-	Add a display for better user experience. The user can
	see the system's state, understand options available to
	them, and tell if a key press registered towards their
	password input (the system displays an asterisk for each
	key pressed).

-	Modularity and maintenance of code. The old code had
	code duplication and function and variable names that were
	difficult to remember. The new code attempts to
	self-document where possible by keeping functions concise
	and understandable. Additionally, the code has been
	split into relevant, separate files so that we don't
	have too many big files.

-	A few new features. The new system is designed to do a
	few new things when the system is unlocked:

	-	The user can change the password after unlocking the
		system.

	-	The user can manually lock the system after
		successfully unlocking it.

Schematic
============================================================

![Schematic diagram](./final_schematic.png)

Tests
============================================================

First-time setup
------------------------------------------------------------

1.	Run the program, or reboot the microcontroller to run
	the program.

2.	When the program first runs, it should display

	~~~
	HELLO:)

	PRESS A TO SET
	UP

	A: NEW PASS
	~~~

3.	Neither LED1 nor LED2 should be lit.

4. 	Continue to [Setting new passwords] to set the password
	for the first time.

Setting new passwords
------------------------------------------------------------

1.	With the system unlocked or in first-time setup (both
	should be tested), press `A`.

2.	The system should display

	~~~
	ENTER NEW
	PASS:



	B: SUBMIT
	~~~

	There should be three blank lines between `PASS:` and
	`B: SUBMIT`.

3.	As the new password is typed, asterisks should display
	in place of the new digits. For example, after entering
	four digits such as `1234`, the display should read

	~~~
	ENTER NEW
	PASS:

	****

	B: SUBMIT
	~~~

4.	Press `B` to submit the new password.

5.	LED2 should glow blue to indicate that a new password
	has been set and the device has been locked and armed.
	When in this state, the system should display

	~~~
	READY!

	:)

	C: UNLOCK
	~~~

Unlocking the system
------------------------------------------------------------

1.	With a password set and the system in its idle state
	(the display should read `READY!`), press `C` to attempt
	to unlock the system. LED2 should turn off.

2.	The system should display

	~~~
	ENTER PASS:



	D: SUBMIT
	~~~

	There should be three blank lines between `ENTER PASS:` and
	`D: SUBMIT`.

3.	Enter the password that the system has been set up with.
	As the password is typed, asterisks should display in place
	of the new digits. For example, after entering four digits
	such as `1234`, the display should read

	~~~
	ENTER PASS:

	****

	D: SUBMIT
	~~~

4.	Press `D` to submit the password entered. If the
	password provided was correct, LED2 should glow green and
	the display should read

	~~~
	HELLO!:)
	SYSTEM IS
	UNLOCKED

	A: NEW PASS
	C: LOCK
	~~~

Entering incorrect passwords {#incorrect_passwords}
------------------------------------------------------------

1.	With a password set and the system in its idle state
	(the display should read `READY!`), press `C` to attempt
	to unlock the system. LED2 should turn off.

2.	The system should display

	~~~
	ENTER PASS:



	D: SUBMIT
	~~~

	There should be three blank lines between `ENTER PASS:` and
	`D: SUBMIT`.

3.	Enter any password that is *not* the correct password.
	As the password is typed, asterisks should display in place
	of the new digits. For example, after entering four digits
	such as `1234`, the display should read

	~~~
	ENTER PASS:

	****

	D: SUBMIT
	~~~

4.	Press `D` to submit the password entered. If the
	password provided was incorrect, LED2 should glow red.
	The display should read

	~~~
	WRONG PASS!

	X TRIES LEFT

	C: TRY AGAIN
	~~~

	A number should be present in place of `X`. For example,
	if the user can enter the password 2 more times before
	disabling the system, the display should read

	~~~
	WRONG PASS!

	2 TRIES LEFT

	C: TRY AGAIN
	~~~

	> 	Note: There is a special test case for when the user
	> 	has one last attempt to enter the correct password
	> 	before the system is disabled. In this case, 
	>	`1 TRY LEFT` should be displayed, like so:
	>
	> 	~~~
	>	WRONG PASS!
	>
	>	1 TRY LEFT
	>
	>	C: TRY AGAIN
	>	~~~

Disabling the system
------------------------------------------------------------

>	Note: It's probably necessary to clarify that a
>	*disabled* system in this context is one that refuses
>	input because too many incorrect passwords have been
>	entered. A "disabled system" in this case refers to a
>	system that is in its alarm state.

1.	Follow the procedure as outlined in [Entering incorrect
	passwords]. In between attempts, be sure to press `C` for
	`TRY AGAIN` (LED2 should turn off). Repeat the procedure
	until the system displays `1 TRY LEFT`. 

2.	Once the system displays `1 TRY LEFT`, follow the
	aforementioned procedure one last time to disable the
	system.

3.	The system should now be disabled and the alarm should
	sound (assuming an alarm *was* connected). Both LED1 and
	LED2 should glow red. The display should show

	~~~
	TOO MANY BAD
	ATTEMPTS

	ALARM IS ON

	UNLOCKING OFF
	~~~

4.	Try pressing various buttons on the keypad. The system
	should *not* respond to any of the keystrokes.

5.	To reset the system, press S1 on the left side of the
	Launchpad. LED1 and LED2 should both turn off. The
	system should return to its idle, locked state:

	~~~
	READY!

	:)

	C: UNLOCK
	~~~

Manually locking the system
------------------------------------------------------------

1.	If the system is currently locked, unlock it by
	following the procedure in [Unlocking the system].

2.	While the system is unlocked, press `C` to manually lock
	the system.

3.	The system should enter its idle, locked mode,
	displaying

	~~~
	READY!

	:)

	C: UNLOCK
	~~~

Demonstration
============================================================

The project is demonstrated in this [YouTube
video](https://youtu.be/M0ssj3l90Us).

Excuse the crackling; I don't know what's wrong with my
microphone.

Source code
============================================================

Here's a link to the [Git
repository](https://codeberg.org/harrisonthorne/microproc-final)
containing the source code for this project. To make things
a little easier to sift through, the following files are the
most important:

-	aliases.h

-	display.h

-	home_security.h

-	interrupts.h

-	io.h

-	main.c

What was the hardest part?
============================================================

The most difficult part of this assignment was probably
implementing the variables `line` and `numChars` (see
[*display.h*](https://codeberg.org/harrisonthorne/microproc-final/src/branch/main/display.h))
to keep track of where the cursor is on the display. The
feature was necessary to prevent the accidental addition or
removal of lines when advancing to the next line or writing
a character on the display. If `line` was not properly
updated after a row of characters overflowed, the next line
of characters after calling `next_line()` may overwrite the
row.
